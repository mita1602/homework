import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.geom.Rectangle2D;
import java.util.Arrays;
import java.util.Random;
 
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
 
@SuppressWarnings("serial")
public class FlyingRects extends JComponent {
  private static int rectsNumber = 10;
  private static double rectWidth = 40;
  private static double rectHeight = 30;
  private static double maxShift = 3;
  private static int delay = 30;
 
  private Rectangle2D[] rects;
  private double[] shiftsX;
  private double[] shiftsY;
  private boolean[] intersects;
 
  public FlyingRects() {
    Timer timer = new Timer(delay, new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        animateRects();
        repaint();
      }
    });
    timer.start();
  }
 
  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    if (rects == null)
      createRects(rectsNumber);
    Graphics2D g2d = (Graphics2D) g;
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                         RenderingHints.VALUE_ANTIALIAS_ON);
    for (int i = 0; i < rects.length; i++) {
      for (int j = i + 1; j < rects.length; j++) {
        // check if rectangles intersects
        if (rects[i].intersects(rects[j])) {
          intersects[j] = intersects[i] = true;
          break;
        }
      }
    }
    for (int i = 0; i < rects.length; i++) {
      if (intersects[i]) {
        g2d.setColor(Color.red);
      } else {
        g2d.setColor(Color.blue);
      }
      g2d.fill(rects[i]);
    }
    Arrays.fill(intersects, false);
  }
 
  private void createRects(int num) {
    Random random = new Random();
    rects = new Rectangle2D[num];
    shiftsX = new double[num];
    shiftsY = new double[num];
    intersects = new boolean[num];
    for (int i = 0; i < num; i++) {
      double x = random.nextDouble() * (getWidth() - rectWidth);
      double y = random.nextDouble() * (getHeight() - rectHeight);
      rects[i] = new Rectangle2D.Double(x, y, rectWidth, rectHeight);
      shiftsX[i] = maxShift * (random.nextDouble() * 2 - 1);
      shiftsY[i] = maxShift * (random.nextDouble() * 2 - 1);
    }
  }
 
  private void animateRects() {
    if (rects == null)
      return;
    for (int i = 0; i < rects.length; i++) {
      Rectangle2D rect = rects[i];
      double dx = shiftsX[i];
      double dy = shiftsY[i];
      double x = rect.getX() + dx;
      double y = rect.getY() + dy;
      double w = rect.getWidth();
      double h = rect.getHeight();
      if (x < 0) {
        x = -x;
        shiftsX[i] = -shiftsX[i];
      }
      if (y < 0) {
        y = -y;
        shiftsY[i] = -shiftsY[i];
      }
      if (x + w > getWidth()) {
        x = (getWidth() - w) * 2 - x;
        shiftsX[i] = -shiftsX[i];
      }
      if (y + h > getHeight()) {
        y = (getHeight() - h) * 2 - y;
        shiftsY[i] = -shiftsY[i];
      }
      rect.setRect(x, y, w, h);
    }
  }
 
  public static void main(String[] args) {
    SwingUtilities.invokeLater(new Runnable() {
      public void run() {
        JFrame frame = new JFrame("Flying Rects");
        frame.getContentPane().add(new FlyingRects());
        frame.setSize(400, 300);
        frame.setVisible(true);
      }
    });
  }
}